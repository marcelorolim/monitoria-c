#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *ler_vetor();

int acha_palavra(char *frase, char *obj);
int comp_palavras(char *str1, char *str2);

int main(){
  char *str1, *str2;
  int i = 0;
  str1 = ler_vetor();
  str2 = ler_vetor();
  printf("conjunto -%s-\n",str1);
  printf(" palavra -%s-\n",str2);

  if(acha_palavra(str1,str2)){
	printf("\n%s\nESTAH contido em \n%s\n",str2,str1);
  }else{
    printf("\n%s\nNAO estah contido em \n%s\n",str2,str1);
  }

  free(str1);
  free(str2);
  return 0;
}

int comp_palavras(char *str1, char *str2){
  int len1 = strlen(str1), len2 = strlen(str2);
  if(len1 != len2) return 0;
  for(; *str1 != '\0' && *str2 != '\0'; str1++,str2++){
	if(*str1 != *str2) return 0;
  }
  return 1;
}

int acha_palavra(char *frase, char *obj){
  char *palavra;
  int lpalavra = 0;
  palavra = (char *) malloc(sizeof(char));
  for(; *frase != ' ' && *frase != '.'; frase++, lpalavra++){
    palavra = (char *) realloc(palavra,(sizeof(char) * lpalavra) + 1);
    palavra[lpalavra] = *frase;
  }
  palavra[lpalavra+1] = '\0';

  /* para andar os espaços em branco ate chegar em uma palavra */
  for(; *frase == ' ';frase++);
  
  if(comp_palavras(palavra,obj)){
	free(palavra);
    return 1;
  }
  free(palavra);

  /* verifica se atingiu o fim da frase*/
  if(*frase == '.' || *frase == '\0'){
	return 0;
  }
  return acha_palavra(frase,obj);
}

char *ler_vetor(){
	char *str;
	unsigned int tamanho = 8, i = 0;
	int c;
	str = (char *) malloc(sizeof(char) * tamanho);
	if(!str){
		exit(1);
	}
	while (( c = fgetc(stdin) ) != EOF && c != '\n' && c != '\0'){
		str[i++] = c;
		if(i == tamanho){
			tamanho += i;
			str = (char *) realloc(str,sizeof(char) * tamanho);
			if(!str){
			  printf("Erro realloc\n");
			  exit(1);
		    }
		}
	}
	str[i] = '\0';
	return realloc( str, sizeof(char)*(strlen(str)+1) );
}

/*

Receber duas strings e verificar se a segunda está contida na primeira.

receber strings, alocando memoria

percorrer a string 1 verificando se a proxima palavra possui o mesmo tamanho que string 2

se o tamanho for igual, comparar as duas palavras letra a letra

se nao for a palavra procurada, continuar a busca na proxima palavra ate .

ex:
  1-sempre em minusculo, separada por espaco e finalizadas por .

  2-palavra unica, toda minuscula só com a-z, sem caracteres especiais

*/
